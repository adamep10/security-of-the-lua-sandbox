--used to allow for a simple redefinition of usually used globals in other script provided by this tool
--if debug metatable getter were accessible, this would not be needed.
local getmetatable = function(v) 
	if(type(v) ~= 'table') then --I know, I know. But hear me out. I checked this will never succeed and merely spams the console. Moreover, this will prevent errors from getting thrown
		return false;
	end
	local res,v = pcall(function() getmetatable(v) end); 
		if(res) then 
			return v; 
		else 
			return false; 
		end;  
	end;

return {

	['type'] = type,
	['tostring'] = tostring,
	['next'] = next,
	['pairs'] = pairs,
	['ipairs'] = ipairs,
	['pcall'] = pcall,
	['xpcall'] = xpcall,
	['getmetatable'] = (debug and debug.getmetatable) or getmetatable, --can be nil with no issues
	['setmetatable'] = setmetatable,--can be nil with no issues
	['getfenv'] = getfenv, --can be nil with no issues
	['insert'] = table.insert,
	['remove'] = table.remove,
	['rawget'] = rawget, --can be nil 
	['select'] = select,
	['print'] = print
};




--[[
function(v) 
	local res,v = pcall(function() getmetatable(v) end); 
		if(res) then 
			return v; 
		else 
			return nil; 
		end;  
	end;
]]