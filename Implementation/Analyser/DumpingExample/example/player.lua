local util = require('envDump');
-- [[
stuff = {}

stuff.coroutine = coroutine;
stuff.math = math;
stuff.os = os;
stuff.string = string;
stuff.table = table;
stuff.openmw = {}
local openmw = stuff.openmw;
openmw.core = require('openmw.core');
openmw.async = require('openmw.async');
openmw.types = require('openmw.types');
openmw.util = require('openmw.util');
openmw.camera = require('openmw.camera');
openmw.input = require('openmw.input');
openmw.interfaces = require('openmw.interfaces');
openmw.nearby = require('openmw.nearby');
openmw.self = require('openmw.self');
openmw.settings = require('openmw.settings');
openmw.storage = require('openmw.storage');
openmw.ui = require('openmw.ui');

--util.log(util.simpleSplitOnType( util.customScan(openmw.storage.playerSection("test")) ) );
print(util.dumpNoWhitespace(util.machineReadableSplit( util.scanEnv(_G) ) ));

return {
    engineHandlers = {
        onKeyPress = function(key)	
            if key.symbol == 'x' then
					require('openmw.util').transform.identity.__gc({}); --crash the game
            end
			if key.symbol == 'v' then
            end
        end
    }
}