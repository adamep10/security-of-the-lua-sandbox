--[[

This file contains the implementation of a smart-ish walker(util.allWalker), 
which attempts to visit all the values which can be accessed via the [] operator on a table or userdata object by making use of the pairs and next functions.

This file also contains a dumper which serializes any value (save for userdata, functions and thread) to a string in a Lua-readable (and potentially human-readable) format.


]]
local conf = require('functionConf');
local type = conf.type;
local pcall = conf.pcall;
local xpcall = conf.xpcall;

local next = conf.next;
local tostring = conf.tostring;
local print = conf.print;
local select = conf.select;


local sets = require('setUtilities');
local util = {}
sets.mergeInto(util, sets);

--using safe pairs may be kinda dumb here, but I'd like as few false negatives as possible.
function util.testPairs(tbl) 

	if(xpcall) then
		return xpcall(function () return pairs(tbl) end, 
						function(...) --[[print(...); print(debug.traceback())]] end);
	else
		return true, pairs(tbl); --assume everything is just fine and hope it goes well. THough it may require skiping pairs on uservalues.
	end;
end;
local testPairs = util.testPairs;
function util.testIPairs(tbl) 
	if(xpcall) then
		return xpcall(function () return ipairs(tbl) end, 
						function(...) return nil end);
	else
		return true, ipairs(tbl); --assume everything is just fine and hope it goes well. THough it may require skiping pairs on uservalues.
	end;
end;
local testIPairs = util.testIPairs;


--if a table were to override it's pairs function, the next fucntion may give me more values.
--this will still not cover the case __index is hiding them.
--TODO: consider if an override iPairs is worth testing as the default one is not as it will only gives a subset of pairs()
--While this could be wrapped in a couroutine to create a generator which could then be used instead of pairs(), that would hurt portability and trying to split that into states would make it pretty much illegible
--TODO: consider doing the for loops manually using table.pack() in case more than two values are ever produced.
function util.allWalker(tbl, callback) 
	--default pairs() return
	local nextFunct = next;
	local pairTable = tbl
	local firstVal = nil;
	--potentially overriden __pairs
	local pairsOK, testNext, testTbl, testVal = testPairs(tbl);
	
	--cannot be parsed via the standard next function
	if(type(tbl) ~= 'table') then
		if(pairsOK) then
			local ok, error = xpcall(function () testNext(testTbl, testVal) end, function(error) print(tostring(error)); print(tostring(testTbl)); end );
			if(ok) then
				for k,v in testNext, testTbl, testVal do
						callback(k,v);
				end;
			end
		end;
		return;
	end;
	
	if(not pairsOK) then -- pairs did not work, default to next interation
		for k,v in nextFunct, pairTable, firstVal do
			callback(k,v);
		end;
	elseif(nextFunct ~= testNext or pairTable ~= testTbl or firstVal ~= testVal) then
		--something is different, just walk this stuff twice and ignore duplicates
		local visitedPairs = {};
		
		--casual walk, assume no side effects
		for k,v in nextFunct, pairTable, firstVal do
			callback(k,v);
			visitedPairs[k] = v;
		end;
		--custom walk now
		for k,v in testNext, testTbl, testVal do
			if(visitedPairs[k] == nil or visitedPairs[k] ~= v) then
				callback(k,v);
			end;
		end;
	else 
		--pairs were OK and returned the same values
		for k,v in nextFunct, pairTable, firstVal do
			callback(k,v);
		end;
	end;
end;

-- The following dumper is very similar to the dumper available in the game Original War. 
-- This is no coincidence, that one was written by me as well. 
local function wrappedDump(o, OFFSET, DONE, ommitWhitespace) --more or less a better table serializer for debugging
	local DONE = DONE or {};
	local typ = type(o);
	if ((typ == 'table' or typ == 'uservalue'))  then
		if(DONE[o]) then
			return tostring(o);
		end;
		DONE[o] = true;
		local padding = ommitWhitespace and '' or '\n' .. string.rep("  ", OFFSET +((OFFSET-1)/2));
		local s = '{ ';
		for k, v in pairs(o) do
		
			s = s .. padding .. '[' .. wrappedDump(k,OFFSET,DONE,ommitWhitespace) .. '] = ';
			s = s .. wrappedDump(v,OFFSET+1,DONE,ommitWhitespace) .. ",";
			
		end;
		padding =  ommitWhitespace and '' or  '\n' .. string.rep("  ", OFFSET-1 +((OFFSET-1)/2));
		return s .. padding .. '}';
	elseif (typ == "string") then
		return string.format("%q", o);
	else
		return tostring(o);
	end;
end;

--dumps any value to a, hopefully, both machine-readable and human readable format;
function util.dump(...)
	local res = '';
	for i=1,select('#',...) do
		res = res .. wrappedDump(select(i,...), 0) .. "\n";
	end;
	return res;
end;
--dumps any value to a, hopefully, both machine-readable and human readable format; 
function util.dumpNoWhitespace(...)
	local res = '';
	for i=1,select('#',...) do
		res = res .. wrappedDump(select(i,...), 0, nil, true) .. "\n";
	end;
	return res;
end;
--log any values to console;
function util.log(...)
	print(util.dump(...));
end;
--log any values to console;
function util.logNoWhitespace(...)
	print(util.dumpNoWhitespace(...));
end;
return util;