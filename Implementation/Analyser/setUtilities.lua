--[[
This file contains utility functions for tables.
]]
local conf = require('functionConf')
local next = conf.next;
local pairs = conf.pairs;
local util = {}
--returns the nnext value as the first return value rather than the next key that the next function returns.
function util.nextValue(tbl, index)
	local k,v = next(tbl, index);
	return v;
end;
--take all key-value pairs in src and write them into dest potentially overwriting anything already there.
function util.mergeInto(dest, src)
	for k,v in pairs(src) do
		dest[k] = v;
	end
end;
--functional profgramming fold
function util.fold(tbl,initial,predicate)
	for k,v in pairs(tbl) do
		initial = predicate(k,v, initial);
	end;
	return initial;
end;
--functional programming map
function util.map(tbl,predicate)
	if(tbl == nil) then
		return
	end;
	local result = {};
	for key,val in pairs(tbl) do
		local k,v = predicate(key,val);
		result[k] = v;
	end;
	return result;
end;
local map = util.map;
--Map which only translates values.
function util.mapValue(tbl,predicate)
	return map(tbl, function(k,v) return k, predicate(v) end)
end;
--will split the table into new tables according the result of the predicate call on the original keys.
function util.bucketByKeys(tbl,predicate)
	local results = {};
	for k,v in pairs(tbl) do
		if(not results[predicate(k)]) then
			results[predicate(k)] = {};
		end;
		results[predicate(k)][k] = v;
	end;
	return results;
end;
--not really a set utility but who cares
--offers a deep-compare of tables instead of the standard identity compare
function util.areEqual(a, b)
	if(type(a) ~= type(b)) then
		return false;
	end
	if(type(a) ~= 'table') then
		return (a == b);
	end
	
	for k,v in pairs(a) do
		if(not areEqual(v,b[k])) then
			return false;
		end;
	end;
	for k,v in pairs(b) do
		if(not areEqual(v,a[k])) then
			return false;
		end;
	end;
	return true;
end
--for two input sets A and B, split into three tables
--	left = values only in A
--  both = values in both
--  right = values only in B
function util.bucketSets(a,b)
	if(not a) then
		return b;
	end;
	if(not b) then
		return a;
	end;

	local both = {};
	local left = {};
	local right = {};


	local visitedKeys = {};
	
	for k,v in pairs(a) do
		visitedKeys[k] = true;
		local rVal = b[k];
		if (areEqual(v, rVal)) then
			both[k] = v;
		else
			left[k] = v;
			right[k] = rVal;
		end;
	end;
	for k,v in pairs(b) do
		if(not visitedKeys[k]) then 
			right[k] = v;
		end;
	end;

	return {
		["left"] = left,["both"] = both,["right"] = right
	}
end
--remove empty tables from the input table
function util.filterEmpty(tbl)
	for k, v in pairs(tbl) do
		if(next(v,nil) == nil) then
			tbl[k] = nil;
		end;
	end;
	return tbl;
end;
--if the input is a table of only one key return the associated value,
--otherwise return the oiginal tbale.
function util.ifSingleFlatten(tbl)
	if(not next(tbl, next(tbl, nil))) then
		return util.nextValue(tbl);
	end
	return tbl;
end;

return util;