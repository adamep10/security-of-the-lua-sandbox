--[[
	
used to allow for a simple redefinition of variables
the dumper and all files it uses depend on the definitions in this file.

It can be used if for whatever resaon the standard functions are not available.


]]
return {

	['type'] = type,
	['tostring'] = tostring,
	['next'] = next,
	['pairs'] = pairs,
	['ipairs'] = ipairs,
	['pcall'] = pcall,
	['xpcall'] = xpcall,
	['getmetatable'] = (debug and debug.getmetatable) or getmetatable, --can be nil with no issues
	['setmetatable'] = setmetatable,--can be nil with no issues
	['getfenv'] = getfenv, --can be nil with no issues
	['insert'] = table.insert,
	['remove'] = table.remove,
	['rawget'] = rawget, --can be nil 
	['select'] = select,
	['print'] = print
};




--[[
function(v) 
	local res,v = pcall(function() getmetatable(v) end); 
		if(res) then 
			return v; 
		else 
			return nil; 
		end;  
	end;
]]