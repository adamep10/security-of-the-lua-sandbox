--[[
This file contains the main part of the dumper as well as the public API.
The public API can be found towards the end of the file.





]]

-- imported functions to allow for quick custom replacements

local config = require('functionConf');

local type = config.type;
local tostring = config.tostring;
local pairs = config.pairs;
local ipairs = config.ipairs;
local pcall = config.pcall;

local getmetatable = config.getmetatable;
local setmetatable = config.setmetatable;
local getfenv = config.getfenv;  
local insert = config.insert;
local remove = config.remove;

local util = {};
local logging = require('genericLogger')
local translators = require('translate')
translators.mergeInto(util, logging);
translators.mergeInto(util, translators);

local enum_source = util.enum_source;
local allWalker = util.allWalker;

function util.shallowClone(tbl)
	local res = {};
	allWalker(tbl, function (k, v) res[k] = v;  end) --using the all walker as it does smart access for all values.
	return res;
end;

toStringIdentifier = util.toStringIdentifier;

--[==[ 
-- this is an unfinished modification of the dumper, it was intended to be used to redefine publicli shared fucnctions and see where and when they are used
-- it proved to be unnecessary.

local function accessLoggerGenerator(name, wraped)

	return  function(...)
		util.log("fn " .. name .. " called with params ", ...);
		local results = table.pack(wraped(...));
		
		return table.unpack(results);
	end;

end;

local function replaceWithAccessLogger(value, path, hasBeenVisited)
	local valueType = type(value);
	if(valueType ~= 'function' or hadBeenVisited) then--while I could be replacing and logging more. Such as pairs callls, right now I only have standard library functons ant tables which are shared.
		return;
	end
	
	local lastNode = path[#path];
	local lastNodeValue = lastNode[1];
	local lastNodeMetatable =  getmetatable(lastNodeValue) or setmetatable
	local lastNodeType = lastNode[2];
	if(lastNodeType == enum_source.specifiedForSearch) then -- do nothing as such value cannot be replaced.
		local nothing = {};
	elseif(lastNodeType == enum_source.env) then--cannot return function
		assert(false);
	elseif(lastNodeType == enum_source.metatable) then
		assert(false);
	elseif(lastNodeType == enum_source.key) then --is a key in table. Not really useful. I cannot replace it as it is the index used. Might be able to play around with pairs() or such if it were a table of callbacks. Don't have a usecase now. skipping
		assert(true);
	elseif(lastNodeType == enum_source.value) then
		pcall(function() lastNodeValue[lastNode[3]] = function(...) util.log("function by key %1 called with params %...", lastNode[3], ...); return value(...); end end)--this WILL crash if no __newindex is specified. I'll solve that when I come to it.
	elseif(lastNodeType == enum_source.pairsFunction) then --this is kinda interesting. This function will then get access to many fun things. TODO: metatable fun
	elseif(lastNodeType == enum_source.ipairsFunction) then
	elseif(lastNodeType == enum_source.pairsTable) then
	elseif(lastNodeType == enum_source.ipairsTable) then
	elseif(lastNodeType == enum_source.pairsValue) then
	elseif(lastNodeType == enum_source.ipairsValue) then
	end;
	
	local soFar = visitedTables[value] or '';
	
	if(hasBeenVisited) then
		visitedTables[value] = soFar .. ' || ' .. pathPoint;
	else
		visitedTables[value] = pathPoint;
	end;
end;
]==]
local commonMetatableValues = {["__index"] = "__index", ["__newindex"] = "__newindex",
    ["__gc"] = "__gc", ["__mode"] = "__mode", ["__len"] = "__len", ["__eq"] = "__eq",
    ["__add"] = "__add", ["__sub"] = "__sub", ["__mul"] = "__mul", ["__mod"] = "__mod", ["__pow"] = "__pow",
    ["__div"] = "__div", ["__idiv"] = "__idiv",
    ["__band"] = "__band", ["__bor"] = "__bor", ["__bxor"] = "__bxor", ["__shl"] = "__shl", ["__shr"] = "__shr",
    ["__unm"] = "__unm", ["__bnot"] = "__bnot", ["__lt"] = "__lt", ["__le"] = "__le",
    ["__concat"] = "__concat", ["__call"] = "__call", ["__close"] = "__close", ["__name"] = "__name",
	["__metatable"] = "__metatable", ["__tostring"] = "__tostring",
	["__pairs"] = "__pairs", ["__ipairs"] = "__ipairs",}
--todo: restrict so non-unique types do not get spammed for metatable.
local function maybeHasMetatable(value)
	return (not getmetatable) or (getmetatable and (getmetatable(value) ~= nil));
end

local function testMetatable(value, recurseFurther, restrictions)
	local doTest = (not restrictions.getmetatable) and getmetatable;
	if(doTest) then
		--try to visit the metatable of any value. This should never throws unless getmetatable has been redefined.
		
		return recurseFurther(getmetatable(value),enum_source.metatable);
	end;
end;

local function testProperties(value, recurseFurther, restrictions)
	
	local doTest = maybeHasMetatable(value) or type(value) == 'table';
	if (not doTest) then--maybe throw an assert if we find a userdata object with no metatable?
		return;
	end;
		
		--There is no point to using rawget as that will not give me more than this will
	local visited = {}
	for _,k in pairs(commonMetatableValues) do --just in case the walking then fails
		local isOk, v = pcall(function() return value[k] end);
		if(isOk) then
			local valueTyp = type(v);
			if( valueTyp ~= 'nil') then
				visited[k] = true;
				recurseFurther(k, enum_source.key, k);
				recurseFurther(v, enum_source.value, k);
			end;
		end;
	end;
	
	allWalker(value, function(k,v)--uses a custom walker to use both default pairs and overloaded __pairs. Also not crash it it does not work.
			if( not visited[k]) then--todo: check if value difers or not.
				recurseFurther(k, enum_source.key, k);
				recurseFurther(v, enum_source.value, k);
			end;
		end)
end;
local basePairsFN = pairs({});
local function testPairsReturns(value, recurseFurther, restrictions)
	local doTest = maybeHasMetatable(value) and not restrictions.pairsRet;
	if (not doTest) then
		return;
	end;
	
	local ok, pairsCallback ,pairsTable, firstVal = util.testPairs(value)
	if(ok) then
		if(pairsCallback ~= basePairsFN) then
			recurseFurther(pairsCallback, enum_source.pairsFunction);
		end;
		if(pairsTable ~= value) then --self being accessible is irrelevant
			recurseFurther(pairsTable, enum_source.pairsTable);
		end;
		if(firstVal ~= nil) then --self being accessible is irrelevant
			recurseFurther(firstVal, enum_source.pairsValue);
		end;
	end;
end;

local baseIPairsFN = ipairs({});
local function testIPairsReturns(value, recurseFurther, restrictions)
	local doTest = maybeHasMetatable(value) and not restrictions.ipairsRet;
	if (not doTest) then
		return;
	end;
	
	local ok, pairsCallback ,pairsTable, firstVal  = util.testIPairs(value) --todo: make a better failsafe than this.
	if(ok) then
		if(pairsCallback ~= baseIPairsFN) then
			recurseFurther(pairsCallback, enum_source.ipairsFunction);
		end;
		if(pairsTable ~= value) then --self being accessible is irrelevant
			recurseFurther(pairsTable, enum_source.ipairsTable);
		end;
		if(firstVal ~= 0) then --self being accessible is irrelevant
			recurseFurther(firstVal, enum_source.ipairsValue);
		end;
	end;
end;

local function testFenv(value, recurseFurther, restrictions)
	local doTest = (not restrictions.getfenv) and getfenv and type(value) == 'function'
	if(doTest) then
		recurseFurther(getfenv(value), enum_source.env);
	end;
end;

local function testUpvalues(value, recurseFurther, restrictions)
	local doTest = debug and debug.getupvalue and type(value) == 'function';
	if(doTest) then
		local index = 1;
		local key, found = debug.getupvalue(value, index);
		while(key ~= nil) do
			if(key == "") then
				recurseFurther(index, enum_source.upvalueNameOrIndex);			
				recurseFurther(found, enum_source.upvalue, index);			
			else
				recurseFurther(key, enum_source.upvalueNameOrIndex);			
				recurseFurther(found, enum_source.upvalue, key);
			end
			index = index +1;
			key, found = debug.getupvalue(value, index);
		end;
	end;
end;

--THIS IS THE MAIN CRAWLING LOGIC
--visits all values accessible via this value
--All values may be visited multiple times, but their childern will be visited only once.
--the different known metatable key values are ***ignored***
--callback(any_value, path, hadBeenVisited)
local visitAllValuesOnce = nil;
function util.visitAllValuesOnce(value, callback, path, visited, restrictions)
	restrictions = restrictions or {};
	visited = visited or error('the visited paraam has to be specified');
	path = path or {{'_unknown_', enum_source.specifiedForSearch}};
	--path, callback and visited should be fine to be captured by a closure and used as such
	local thisPathSegment = {value, enum_source.specifiedForSearch, nil};
	local function recurseFurther(value, enum, variable)
		thisPathSegment[2] = enum;
		thisPathSegment[3] = variable;
		visitAllValuesOnce(value, callback, path, visited, restrictions);
	end;
	local typ = type(value);
	if(typ == 'nil') then
		return
	end;
	local hadBeenVisited = visited[value];
	
	-- I have just been visited!
	callback(value,path,hadBeenVisited);
	
	--no recursive walking of visited values 
	if(hadBeenVisited) then
		return;
	end	
	
	local lastNode = path[#path];
	
	visited[value] = true;
	--add self to path and visit children.
	insert(path,thisPathSegment);
	testMetatable(value, recurseFurther, restrictions);
	testProperties(value, recurseFurther, restrictions);
	testFenv(value, recurseFurther, restrictions);
	testUpvalues(value, recurseFurther, restrictions);
	
	--try to call the pairs and ipairs method just in case it is overriden in a very, VERY bad way.
	--only do this after the rest has been searched as I'd assume this resulting in many false positives and will be much less legible in the log.
	local origRestrictions = restrictions.pairsRet;
	restrictions.pairsRet = origRestrictions or (lastNode[2] ~= enum_source.pairsTable and lastNode[2] ~= enum_source.ipairsTable);
	
	testPairsReturns(value,recurseFurther, restrictions)
	restrictions.pairsRet = origRestrictions;
	
	local origRestrictions = restrictions.ipairsRet;
	restrictions.ipairsRet = origRestrictions or (lastNode[2] ~= enum_source.pairsTable and lastNode[2] ~= enum_source.ipairsTable);
	
	testIPairsReturns(value,recurseFurther, restrictions)
	restrictions.ipairsRet = origRestrictions;

	remove(path);
	return;
end;
visitAllValuesOnce = util.visitAllValuesOnce;
local visitedTables = {}
local function logPaths(value, path, hasBeenVisited)
	local soFar = visitedTables[value] or {};
	insert(soFar, util.shallowClone(path[#path]));
	visitedTables[value] = soFar;
end

local function visitStartGen(visited)
	visited[visitedTables] = true;--needed as otherwise the __pairs key makes the value get called and we do not want that. Moreover it is eaningless in the log after all.
	visited[visited] = true;
	return function (value, name)
		visitedTables[name] = {{}};
		util.visitAllValuesOnce(value, logPaths, {{name ,enum_source.specifiedForSearch}},visited);
	end;
end;

local function reset()
	local res = visitedTables;
	visitedTables = {};
	return res;
end;

--------------------------------------------------------------------------------------------------PUBLIC API BELLOW-------------------------------------------------------------------------------------------------------------------

--this function does a "smart" scan of the environment.
--it adds most data types to the search to increase the surface area in a non-invasive way.
--if getfenv is accessible, the global environment is ignored.
--this is done as in all my usecases, the getfenv function was manually added to the sandbox and leaked way too much.
function util.scanEnv(glob, includeGlobal)
	local visited = {};
	if(getfenv and not includeGlobal) then
		log("with global env as:", tostring(getfenv(0)), "being ignored");
		visited[getfenv(0)] = true;
	end;
	local visitStart = visitStartGen(visited);
	
	visitStart(glob, '.');
	visitStart('a', 'str.');
	visitStart(1, 'nrD.');
	visitStart(1.1, 'nrF.');
	visitStart(true, 'bool.');
	if(getmetatable) then
		visitStart(getmetatable(nil), 'nil_metatable.');
	end
	visitStart(function () return 0;end, 'fn.');
	visitStart({}, 'tbl.');
	if(type(coroutine) ~= 'nil' and pcall(coroutine.create, function() return 1 end)) then
		visitStart(coroutine.create( function() return 1; end), 'cor.');
	end;
	if(debug) then
		if(debug.getregistry) then
			visitStart(debug.getregistry(), 'debug.getregistry()');
		end
	end;
	--todo: if it seems relevant add the calling stack from the debug package in as well

	return reset();
end;
--this is a more customisable version of the environment scan. 
--All values which are to be searched and all values which are to be ignored have to be explicitly listed
function util.customScan(tbl, visited)
	visited =  visited or {};
	local visitStart = visitStartGen(visited);
	visitStart(tbl, "__ENV");
	return reset();
end;

-- This function takes a table which was created by the scan.
-- A more or less human-readable dump of the values found in the table is returned.
function util.simpleSplitOnType(tbl)
	local tmp = util.bucketByKeys(
						util.map(tbl, 
							function(key, value) 
									value = util.translateSources(value);
									if(type(key) == "table") then return {tostring(key)},value end; 
								return key, value end)
						,type);
	tmp['table'] = util.map(tmp['table'], function (key, value) return key[1], value end);
	return tmp;
end;

-- This function takes a table which was created by the scan.
-- This returns the full dump later used by the analyser.
function util.machineReadableSplit(tbl)

	return {
	["index"] = util.mapValue(util.bucketByKeys( tbl ,type),function(v) --as we will want to search for shared tables and such, not really caring for the parents
			return util.map(v, function (key, parents) return toStringIdentifier(key), tostring(key) end) 
			end),
	
	["parents"] = util.map(tbl, function(id,parents) 
							local res =  {};
								for k,node in pairs(parents) do
									local found = res[toStringIdentifier(node[1])] or {};
									insert(found, {toStringIdentifier(node[1]), node[2], { type(node[3]) ,tostring(node[3]), toStringIdentifier(node[3])} } ) ;
									res[toStringIdentifier(node[1])] = found;
								end;
							return toStringIdentifier(id),  res;
							
							end) 
	}
end;

--[[ this si here to give an example of how an environment may be acquired across versions
local glob = _ENV;
if (not glob) then
	glob = getfenv(1);
end;
]]
return util;
