assert(string.reverse('sa') == 'as');
assert(string.len('sa') == 2);
assert(not pcall( function () 
			local fn, tabl = pairs(string); 
				tabl.reverse = function(...) return 'dummy' end;
			assert(string.reverse('sa') == 'dummy')  	
				end),
		'pairs has leaked a mutable table')
assert(not pcall( function () 
				local fn, tabl = ipairs(string); 
				tabl.len = function(...) return 'dummy2' end;
			assert(string.len('sa') == 'dummy2' )  	
				end),
		'ipairs has leaked a mutable table')
assert(string.reverse('sa') == 'as');
assert(string.len('sa') == 2);

print('Everything is fine for the pairs function');

return {
}